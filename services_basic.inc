<?php

/**
 * @file
 * Include file for services_basic module.
 */

/**
 * Authenticates a call using Basic HTTP authentication to verify the request.
 *
 * @param array $settings
  *  The settings for the authentication module.
 * @param array $method
 *  The method that's being called
 * @param array $args
 *  The arguments that are being used to call the method
 * @return void|string
 *  Returns nothing, or a error message if authentication fails
 */
function _services_basic_authenticate_call($settings, $method, $args) {
  global $user;
  if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
    $name = $_SERVER['PHP_AUTH_USER'];
    $pass = $_SERVER['PHP_AUTH_PW'];

    // Note the short circuit preventing authentication of blocked users.
    if (user_is_blocked($name) || !($uid = user_authenticate($name, $pass))) {
      $site_name = trim(variable_get('site_name', 'Drupal'));
      $realm = mime_header_encode($site_name);
      drupal_add_http_header('Status', '401 Unauthorized');
      drupal_add_http_header('WWW-Authenticate', 'Basic realm="' . $realm .'"');
    }
    else {
      $user = user_load($uid);
    }
  }
}
